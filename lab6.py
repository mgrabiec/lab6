# Define 
file1 = open('myfile.txt', 'w')
print('Enter lines to save input :wq to save and exit\n\n')


lineToSave=str(input())
# Get linest unitil user input :wq
while(lineToSave!=':wq'):
    file1.writelines(lineToSave+'\n')
    lineToSave=str(input())

# Close file
file1.close()
  
# Opening file 
file1 = open('lines.txt', 'r') 
count = 0
  
# Using for loop 
print("\nSaved lines:\n") 
for line in file1: 
    count += 1
    print("Line{}: {}".format(count, line.strip())) 
  
# Closing files 
file1.close() 
